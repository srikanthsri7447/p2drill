const data = require('./data')

const mailsCount = data.reduce((mailsCount,eachPerson) =>{

    let extension = eachPerson.email.split('.').pop()

    if(mailsCount.hasOwnProperty(extension)){
        mailsCount[extension] += 1
    }
    else{
        mailsCount[extension] = 1
    }
    
    return mailsCount
},{})

console.log(mailsCount)