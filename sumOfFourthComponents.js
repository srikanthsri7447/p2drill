const ipAddressArray = require('./ipAddress');

const sumOfSecondComponents = ipAddressArray.reduce((acc, eachArray) => {
    const sum = acc + eachArray.ip_address[3]
    return sum
}, 0)

console.log(sumOfSecondComponents)