const data = require('./data')

const ipAddressArray = data.map((eachPerson) => {

    const outputArray = []

    const splitedArray = (eachPerson.ip_address).split('.')

    for (let item of splitedArray) {
        outputArray.push(Number(item))
    }

    eachPerson.ip_address = outputArray

    return eachPerson
})

console.log(ipAddressArray)

module.exports = ipAddressArray;