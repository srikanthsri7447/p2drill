const data = require('./data');

const sortedData = data.sort((acc, currentPerson) => {

    let firstPerson = acc.first_name.toLowerCase()
    let secondPerson = currentPerson.first_name.toLowerCase()

    if (firstPerson > secondPerson) {
        return -1;
    }
    if (firstPerson < secondPerson) {
        return 1;
    }
    return 0;
});

console.log(sortedData)